<?php

namespace App\Http\Controllers;


use App\Category;
use App\Question;
use Illuminate\Http\Request;
use Validator;

/**
 * Class QuestionController.
 *
 * @package App\Http\Controllers
 */
class QuestionController extends Controller
{

    /**
     * QuestionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Adding question from admin view.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addQuestion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'question' => 'required',
            'author' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect('/admin/questions')
                ->withErrors($validator)
                ->withInput();
        }
        $this->createQuestion($request);
        return redirect()->route('questions');
    }

    /**
     * Adding question from public view.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addQuestionPublic(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'question' => 'required',
            'author' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        }
        $this->createQuestion($request);
        return redirect()->route('index');
    }

    /**
     * Create question from request.
     *
     * @param Request $request
     */
    private function createQuestion(Request $request)
    {
        $category_id = $request->get('category_id');
        $category = Category::find($category_id);
        $author = $request->get('author');
        $question_text = $request->get('question');

        $category->questions()->create([
            'question_text' => $question_text,
            'answer' => null,
            'author' => $author,
            'status' => 'awaiting',
        ]);
    }

    /**
     * Editing question view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editQuestionView(Request $request)
    {
        $statuses = array('awaiting', 'published', 'hidden');
        $question = Question::find($request->get('question_id'));
        $categories = Category::all();
        return view('editquestion', array('question' => $question, 'categories' => $categories, 'statuses' => $statuses));
    }

    /**
     * Editing/updating question.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editQuestion(Request $request)
    {
        $this->validate($request, [
            'author' => 'required',
            'answer' => 'required',
            'category_id' => 'required',
        ]);

        $answer = $request->get('answer');
        $status = $request->get('status');
        $author = $request->get('author');
        $question = Question::find($request->get('question_id'));
        $category_id = $request->get('category_id');
        $category = Category::find($category_id);

        $question->category_id = $category->id;
        $question->answer = $answer;
        $question->author = $author;
        $question->status = $status;

        $question->update();
        return redirect()->route('questions');
    }

    /**
     * Delete question.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteQuestion(Request $request)
    {
        $question = Question::find($request->get('question_id'));
        $question->delete();
        return redirect()->route('questions');
    }

}