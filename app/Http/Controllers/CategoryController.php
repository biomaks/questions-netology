<?php

namespace App\Http\Controllers;


use App\Category;
use Illuminate\Http\Request;
use Validator;

/**
 * Class CategoryController.
 *
 *
 * @package App\Http\Controllers
 */
class CategoryController extends Controller
{

    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Add category handler.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/admin#category')
                ->withErrors($validator)
                ->withInput();
        }

        $category = Category::create(array(
            'category_name' => $request->get('category_name')
        ));

        $category->save();
        return redirect()->route('categories');
    }

    /**
     * delete category handler.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteCategory(Request $request)
    {
        $category_id = $request->get('category_id');
        $category = Category::find($category_id);
        $category->delete();
        return redirect()->route('categories');
    }

    /**
     * Edit category view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editCategoryView(Request $request)
    {
        $category = Category::find($request->get('category_id'));
        return view('editcategory', array('category' => $category));
    }

    /**
     * Edit category handler.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editCategory(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'category_name' => 'required',
        ]);
        $category = Category::find($request->get('category_id'));
        $category_name = $request->get('category_name');
        $category->category_name = $category_name;
        $category->save();
        return redirect()->route('categories');
    }

}