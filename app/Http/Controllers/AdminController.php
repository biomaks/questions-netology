<?php

namespace App\Http\Controllers;


use App\Category;
use App\Question;
use App\User;
use Illuminate\Http\Request;
use Validator;

/**
 * Class AdminController.
 *
 *
 * @package App\Http\Controllers
 */
class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = User::all();
        return view('admin_main', array('admins' => $admins));
    }

    /**
     * Categories list.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function categories()
    {
        $categories = Category::all();
        return view('categories', array('categories' => $categories));
    }

    /**
     * questions list.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function questions()
    {
        $questions = Question::all();
        $categories = Category::all();
        return view('questions', array('questions' => $questions, 'categories' => $categories));
    }

    /**
     * add new admin view.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addAdminView()
    {
        return view('addadmin');
    }

    /**
     * Edit admin view.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editAdminView(Request $request)
    {
        $admin_id = $request->get('admin_id');
        $admin = User::find($admin_id);
        return view('editadmin', array('admin' => $admin));
    }

    /**
     * Adding admin handler.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addAdmin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:3|confirmed',

        ]);

        if ($validator->fails()) {
            return redirect('/admin/addadmin')
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::create([
            'username' => $request['name'],
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);
        $user->save();

        return redirect()->route('admin');
    }

    /**
     * Update admin from request.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editAdmin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'password' => 'required|min:3|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/editadmin')
                ->withErrors($validator)
                ->withInput();
        }

        $admin_id = $request->get('admin_id');
        $admin = User::find($admin_id);
        $admin->password = bcrypt($request['password']);
        $admin->save();
        return redirect()->route('admin');
    }

    /**
     * Delete admin.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteAdmin(Request $request)
    {
        $admin_id = $request->get('admin_id');
        $admin = User::find($admin_id);
        $admin->delete();
        return redirect()->route('admin');
    }


}