<?php

namespace App\Http\Controllers;


use App\Category;

class MainPageController extends Controller
{

    public function index()
    {
        $categories = Category::whereHas('questions', function ($question) {
            $question->where('status', '=', 'published');
        })->get();
        $allCategories = Category::all();
        return view('home', array('categories' => $categories, 'allCategories' => $allCategories));

    }

}