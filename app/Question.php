<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['question_text', 'answer', 'status', 'author'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}