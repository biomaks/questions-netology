@extends('layouts.admin')
@section('content')
    <nav class="nav">
        <div class="nav-wrapper">
            <a href="{{ url('/admin')}}" class="brand-logo left">Admin site</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="{{ url('/')}}">Home page</a></li>
                <li><a href="{{ url('/admin')}}">Admins list</a></li>
                <li class="active"><a href="{{ url('/admin/categories')}}">Categories</a></li>
                <li><a href="{{ url('/admin/questions')}}">Questions</a></li>
                <li>
                    <a href="#">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
                {{--</li>--}}
            </ul>
            <ul class="tabs tabs-transparent">
            </ul>
        </div>
    </nav>
    <div class="container">

        <div class="row">
            <div class="section">
                <h5>Categories management</h5>
                <div class="divider"></div>
            </div>
            <div id="categories">
                @if (count($errors) > 0)
                    <div class="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="col s12" method="post" action="{{url('/admin/addcategory')}}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="add_category" type="text" class="validate" name="category_name" required>
                            <label for="add_category" data-error="wrong" data-success="right">Add category
                                name</label>
                        </div>
                        <div class="input-field col s1">
                            <button class="btn waves-effect waves-light" type="submit" name="action">Add
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </form>

                <div class="section">
                    <div class="col s8">
                        <table class="bordered">
                            <thead>
                            <tr>
                                <th data-field="name"> Category Name</th>
                                <th class="right-align" data-field="action">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>
                                        {{$category->category_name}}
                                    </td>

                                    <td class="right">
                                        <a href="{{url('/admin/editcategory')}}?category_id={{$category->id}}" class="btn-floating tooltipped" data-position="top" data-delay="50"
                                           data-tooltip="Edit category?"><i class="material-icons">mode_edit</i></a>
                                        <a href="{{url('/admin/deletecategory')}}?category_id={{$category->id}}" class="btn-floating tooltipped red" data-position="top" data-delay="50"
                                           data-tooltip="Delete category?"><i class="material-icons">delete</i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

