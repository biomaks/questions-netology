<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="{{url('css/reset.css')}}"> <!-- CSS reset -->
    <link rel="stylesheet" href="{{url('css/style.css')}}"> <!-- Resource style -->
    <link rel="stylesheet" href="{{url('css/styles.css')}}"> <!-- Resource style -->
    <script src="{{url('js/modernizr.js')}}"></script> <!-- Modernizr -->
    <title>FAQ</title>
</head>
<body>
@yield('content')
<!-- Scripts -->
<script src="{{url('js/jquery-2.1.1.js')}}"></script>
<script src="{{url('js/jquery.mobile.custom.min.js')}}"></script>
<script src="{{url('js/main.js')}}"></script> <!-- Resource jQuery -->

</body>
</html>
