@extends('layouts.admin')
@section('content')
    <nav class="nav">
        <div class="nav-wrapper">
            <a href="{{ url('/admin')}}" class="brand-logo left">Admin site</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="{{ url('/')}}">Home page</a></li>
                <li><a href="{{ url('/admin')}}">Admins list</a></li>
                <li><a href="{{ url('/admin/categories')}}">Categories</a></li>
                <li class="active"><a href="{{ url('/admin/questions')}}">Questions</a></li>
                <li>
                    <a href="#">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
                {{--</li>--}}
            </ul>
            <ul class="tabs tabs-transparent">
            </ul>
        </div>
    </nav>
    @if(count($categories) > 0)
        <div class="container">
            <div id="questions">
                <div class="row">
                    <div class="section">
                        <h5>Questions management</h5>
                        <p>Add question</p>
                        <div class="divider"></div>
                    </div>
                </div>
                @if (count($errors) > 0)
                    <div class="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <form action="{{ url('/admin/addquestion')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="author" value="{{Auth::user()->name }}">
                        <div class="input-field">
                            <label for="question">question</label>
                            <textarea class="materialize-textarea" id="question" type="text" name="question"></textarea>

                        </div>


                        <div class="input-field">
                            <select name="category_id" id="category">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                                @endforeach
                            </select>
                            <label for="category">Select category</label>
                        </div>

                        <div class="input-field col s1">
                            <button class="btn waves-effect waves-light" type="submit" name="action">Save
                                <i class="material-icons right">library_add</i>
                            </button>
                        </div>


                    </form>
                </div>

                @foreach($categories as $category)
                    @if($category->questions->count() > 0)
                        <div class="row">
                            <div class="section">
                                <h5>Category: {{$category->category_name}}</h5>
                                <div class="divider"></div>
                            </div>
                            <table class="bordered highlight">
                                <thead>
                                <tr>
                                    <th>Created</th>
                                    <th>Author</th>
                                    <th>Question text</th>
                                    <th>Answer</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                @foreach($category->questions as $question)
                                    <tr>
                                        <td>{{ date( 'M d, Y H:i ', strtotime($question->created_at))}}</td>
                                        <td>{{$question->author}}</td>
                                        <td>{{$question->question_text}}</td>
                                        <td>{{$question->answer}}</td>
                                        <td>{{$question->status}}</td>
                                        <td>
                                            <a href="{{url('/admin/editquestion')}}?question_id={{$question->id}}"
                                               class="btn-floating tooltipped" data-position="top" data-delay="50"
                                               data-tooltip="Edit question?"><i class="material-icons">mode_edit</i></a>
                                            <a href="{{url('/admin/deletequestion')}}?question_id={{$question->id}}"
                                               class="btn-floating tooltipped red" data-position="bottom"
                                               data-delay="50"
                                               data-tooltip="Delete question?"><i class="material-icons">delete</i></a>
                                        </td>
                                    </tr>

                                @endforeach

                            </table>

                        </div>

                    @endif
                @endforeach


            </div>
        </div>
    @else
        You should create at least one category first.
    @endif
@endsection