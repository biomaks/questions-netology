@extends('layouts.app')

@section('content')
    <header>
        <h1><a href="{{url('/')}}">FAQ</a></h1>
    </header>
    <section class="cd-faq">
        <ul class="cd-faq-categories">
            @foreach($categories as $category)
                <li><a class="selected" href="#{{$category->category_name}}">{{$category->category_name}}</a></li>
            @endforeach
        </ul> <!-- cd-faq-categories -->

        <div class="cd-faq-items">
            @foreach($categories as $category)
                <ul id="{{$category->category_name}}" class="cd-faq-group">
                    <li class="cd-faq-title"><h2>{{$category->category_name}}</h2></li>
                    @foreach($category->questions as $question)
                        @if($question->status == 'published')
                            <li>
                                <a class="cd-faq-trigger" href="#0">{{$question->question_text}}</a>
                                <div class="cd-faq-content">
                                    <p>{{$question->answer}}</p>
                                </div> <!-- cd-faq-content -->
                            </li>
                        @endif
                    @endforeach
                    @endforeach
                </ul> <!-- cd-faq-group -->
                <ul class="cd-faq-group">
                    <li class="cd-faq-title"><h2>Add your question</h2></li>
                    <li>
                        <a class="cd-faq-trigger" href="#0">Add your question</a>
                        <div class="cd-faq-content">
                            <div id="contact-form">
                                <form method="post" action="{{url('/addquestion')}}">
                                    {{csrf_field()}}
                                    <div>
                                        <label for="name">
                                            <input type="text" id="name" name="author" value="" placeholder="Your Name"
                                                   required="required" tabindex="1" autofocus="autofocus"/>
                                        </label>
                                    </div>
                                    <div>
                                        <label for="email">
                                            <input type="email" id="email" name="email" value=""
                                                   placeholder="Your Email" tabindex="2" required="required"/>
                                        </label>
                                    </div>
                                    <div>
                                        <label for="subject">
                                            <span>Category</span>
                                            <select id="subject" name="category_id">
                                                @foreach($allCategories as $category)
                                                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                                                @endforeach
                                            </select>
                                        </label>
                                    </div>
                                    <div>
                                        <label for="message">
                                            <textarea id="message" name="question"
                                                      placeholder="Your question" tabindex="5"
                                                      required="required"></textarea>
                                        </label>
                                    </div>
                                    <div>
                                        <button name="submit" type="submit" id="submit">SEND</button>
                                    </div>
                                </form>

                            </div>
                        </div> <!-- cd-faq-content -->
                    </li>
                </ul> <!-- cd-faq-group -->
        </div> <!-- cd-faq-items -->
        <a href="#0" class="cd-close-panel">Close</a>
    </section> <!-- cd-faq -->
    <script src="{{url('js/main.js')}}"></script>
@endsection
