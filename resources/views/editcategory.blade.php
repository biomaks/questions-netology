@extends('layouts.admin')
@section('content')
    <nav class="nav">
        <div class="nav-wrapper">
            <a href="{{ url('/admin')}}" class="brand-logo left-align">Admin site</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="{{ url('/')}}">Home page</a></li>
                <li><a href="{{ url('/admin')}}">Admins list</a></li>
                <li class="active"><a href="{{ url('/admin/categories')}}">Categories</a></li>
                <li><a href="{{ url('/admin/questions')}}">Questions</a></li>
                <li>
                    <a href="#">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
            <ul class="tabs tabs-transparent">
            </ul>
        </div>
    </nav>

    <div class="container">

        <div id="editcategory">

            <div class="row">
                <div class="section">
                    <h5>Edit category: {{$category->category_name}}</h5>
                    <div class="divider"></div>
                </div>
                <div class="col s6">
                    <form class="" role="form" method="POST" action="{{ url('/admin/editcategory') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="category_id" value="{{$category->id}}">
                        <div class="{{ $errors->has('category_name') ? ' error' : '' }}">

                            <div class="input-field">
                                <label for="category_name" class="col s6 ">New category name</label>
                                <input name="category_name" id="category_name" type="text" class="form-control"
                                       value="{{ $category->category_name }}"
                                       required>
                                @if ($errors->has('category_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="input-field">
                        <a href="{{url('/admin/categories')}}" class="btn red">
                            Cancel
                        </a>
                            <div class="col s6">
                                <button type="submit" class="btn">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
