@extends('layouts.admin')
@section('content')
    <nav class="nav">
        <div class="nav-wrapper">
            <a href="{{ url('/admin')}}" class="brand-logo left">Admin site</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="{{ url('/')}}">Home page</a></li>
                <li><a href="{{ url('/admin')}}">Admins list</a></li>
                <li><a href="{{ url('/admin/categories')}}">Categories</a></li>
                <li class="active"><a href="{{ url('/admin/questions')}}">Questions</a></li>
                <li>
                    <a href="#">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
                {{--</li>--}}
            </ul>
            <ul class="tabs tabs-transparent">
            </ul>
        </div>
    </nav>
    <div class="container">
        <div id="question_form">
            <div class="row">

                <div class="section">
                    <h5>Edit question</h5>
                    <div class="divider"></div>
                </div>

                @if (count($errors) > 0)
                    <div class="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="{{url('/admin/editquestion')}}" method="post">
                    {{csrf_field()}}
                    <input name="question_id" type="hidden" value="{{$question->id}}">

                    <div class="input-field">
                        <label for="questionarea">Question text</label>
                        <textarea class="materialize-textarea" name="question"
                                  id="questionarea">{{$question->question_text}}</textarea>
                    </div>

                    <div class="input-field">
                        <label for="answerarea">Answer text</label>
                        <textarea class="materialize-textarea" name="answer"
                                  id="answerarea">{{$question->answer}}</textarea>
                    </div>
                    <div class="input-field">
                        <label for="author" data-error="wrong" data-success="right">Author name</label>
                        <input type="text" class="validate" name="author" id="author" value="{{$question->author}}">
                    </div>

                    <div class="input-field">
                        <select name="category_id" id="category">
                            @foreach($categories as $category)
                                @if($category->category_name == $question->category->category_name)
                                    <option selected value="{{$category->id}}">{{$category->category_name}}</option>
                                @else
                                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                                @endif
                            @endforeach
                        </select>
                        <label for="category">Select category</label>
                    </div>
                    <div class="input-field">
                        <select name="status" id="status">
                            @foreach($statuses as $status)
                                @if($status == $question->status)
                                    <option selected value="{{$status}}">{{$status}}</option>
                                @else
                                    <option value="{{$status}}">{{$status}}</option>
                                @endif
                            @endforeach
                        </select>
                        <label for="status">Select status</label>
                    </div>

                    <div class="input-field col s1">
                        <button class="btn waves-effect waves-light" type="submit" name="action">Save
                            <i class="material-icons right">library_add</i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection