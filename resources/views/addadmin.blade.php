@extends('layouts.admin')
@section('content')
    <nav class="nav">
        <div class="nav-wrapper">
            <a href="{{ url('/admin')}}" class="brand-logo left-align">Admin site</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="{{ url('/')}}">Home page</a></li>
                <li class="active"><a href="{{ url('/admin')}}">Admins list</a></li>
                <li><a href="{{ url('/admin/categories')}}">Categories</a></li>
                <li><a href="{{ url('/admin/questions')}}">Questions</a></li>
                <li>
                    <a href="#">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
                {{--</li>--}}
            </ul>
            <ul class="tabs tabs-transparent">
            </ul>
        </div>
    </nav>

    <div class="container">

        <div id="addadmin">

            <div class="row">
                <div class="section">
                    <h5>Create new administrator</h5>
                    <div class="divider"></div>
                </div>
                <div class="col s6">
                    <form class="" role="form" method="POST" action="{{ url('/admin/addadmin') }}">
                        {{ csrf_field() }}

                        <div class="{{ $errors->has('name') ? ' error' : '' }}">

                            <div class="input-field">
                                <label for="name" class="col s6 ">Name</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                       required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="{{ $errors->has('email') ? ' error' : '' }}">

                            <div class="input-field">
                                <label for="email" class="col s6 ">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ old('email') }}"
                                       required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="{{ $errors->has('password') ? ' error' : '' }}">

                            <div class="input-field">
                                <label for="password" class="col s6 ">Password</label>
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="input-field">
                            <label for="password-confirm" class="col s6 ">Confirm Password</label>
                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation" required>
                        </div>

                        <div class="input-field">
                            <div class="col s6">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
