@extends('layouts.admin')
@section('content')

    <nav class="nav">
        <div class="nav-wrapper">
            <a href="{{ url('/admin')}}" class="brand-logo left-align">Admin site</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="{{ url('/')}}">Home page</a></li>
                <li class="active"><a href="{{ url('/admin')}}">Admins list</a></li>
                <li><a href="{{ url('/admin/categories')}}">Categories</a></li>
                <li><a href="{{ url('/admin/questions')}}">Questions</a></li>
                <li>
                    <a href="#">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
                {{--</li>--}}
            </ul>
            <ul class="tabs tabs-transparent">
            </ul>
        </div>
    </nav>

    <div class="container">
        <div id="admins">
            <div class="row">
                <div class="section">
                    <h5>Admin management</h5>
                    <div class="divider"></div>
                </div>
            </div>
            <div>
                <a class="btn" href="{{ url('/admin/addadmin') }}">Add new admin</a>
            </div>


            <div class="section">
                <table class="striped bordered">
                    <thead>
                    <tr>
                        <th data-field="id">id</th>
                        <th data-field="name"> name</th>
                        <th data-field="email"> email</th>
                        <th data-field="action">actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($admins as $admin)
                        <tr>
                            <td>{{ $admin->id }}</td>
                            <td>{{$admin->name}}</td>
                            <td>{{$admin->email}}</td>
                            <td>
                                <a class="btn-floating tooltipped" data-position="top" data-delay="50" data-tooltip="Edit admin?" href="{{url('/admin/editadmin')}}?admin_id={{$admin->id}}"><i
                                            class="material-icons">mode_edit</i></a>
                                <a class="btn-floating tooltipped red" data-position="top" data-delay="50" data-tooltip="Delete admin?" href="{{url('/admin/deleteadmin')}}?admin_id={{$admin->id}}"><i class="material-icons">delete</i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>


    </div>
@endsection
