<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'MainPageController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/admin', 'AdminController@index')->name('admin');

Route::get('/admin/categories', 'AdminController@categories')->name('categories');
Route::get('/admin/editcategory', 'CategoryController@editCategoryView');
Route::get('/admin/deletecategory', 'CategoryController@deleteCategory');
Route::get('/admin/questions', 'AdminController@questions')->name('questions');
Route::get('/admin/addadmin', 'AdminController@addAdminView')->name('addadminveiw');
Route::get('/admin/editadmin', 'AdminController@editAdminView')->name('editadminview');
Route::get('/admin/deleteadmin', 'AdminController@deleteAdmin');
Route::get('/admin/editquestion', 'QuestionController@editQuestionView')->name('editquestionview');
Route::get('/admin/deletequestion', 'QuestionController@deleteQuestion');

Route::post('/admin/addcategory', 'CategoryController@addCategory');
Route::post('/admin/addquestion', 'QuestionController@addQuestion');
Route::post('/addquestion', 'QuestionController@addQuestionPublic');
Route::post('/admin/addadmin', 'AdminController@addAdmin');
Route::post('/admin/editadmin', 'AdminController@editAdmin');
Route::post('/admin/editquestion', 'QuestionController@editQuestion');
Route::post('/admin/editcategory', 'CategoryController@editCategory');
